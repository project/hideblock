(function ($) {

  Drupal.hideblock = Drupal.hideblock || {};

  Drupal.behaviors.hideblock = {
    attach: function (context, settings) {
      $('.hideblock').once('hideblock').each(function () {
        var id = this.id.split("-").pop();
        var titleElt = $(this)[0];
        if (titleElt.children.length > 0) {
          titleElt.target = $(this).siblings().not($('.contextual'));
          $(titleElt)
          .wrapInner('<button type="button" class="close" aria-label="Close">')
          .click(function (e) {
            $(this).parent('.block').hide();
            var url = settings.hideblock.path.split("/").
              slice(0, -1).join("/") + "/" + id;
            if (!url || url === undefined) {
              return false;
            }
            var currentPath = settings.hideblock.current_path;
            $.ajax({
              url: url + '?page=' + currentPath,
              done: function (data) {
                $(this).parent('.block').hide();
              }
            });
          });
        }
      });
    }
  }
})(jQuery);
