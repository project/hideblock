<?php

namespace Drupal\hideblock\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\block\Entity\Block;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines HideBlockController class.
 */
class HideBlockController extends ControllerBase {

  /**
   * Return the output.
   *
   * @param \Drupal\block\Entity\Block $block
   *   Block class object param.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return response object.
   */
  public function content(Block $block, Request $request) {
    $page = $request->get('page');
    $response = new Response();
    // Validate the block and user permissions.
    if ($block && $this->currentUser()
      ->hasPermission('administer hide block')) {
      // Process block visibility.
      $this->processBlock($block, $page);

      return $response;
    }

    return $response->create(
      'FALSE',
      Response::HTTP_BAD_REQUEST)
      ->setSharedMaxAge(300);
  }

  /**
   * Process block operation.
   */
  protected function processBlock(&$block, $page) {
    $visibility = $block->getVisibility();
    if ($visibility['request_path']['negate']) {
      $visibility['request_path']['pages'] .= ' ' . $page;
    }
    elseif (!empty($visibility['request_path']['pages']) &&
      $visibility['request_path']['pages'] !== $page) {
      $visibility['request_path']['pages'] = str_replace($page, '', $visibility['request_path']['pages']);
    }
    else {
      $visibility['request_path']['negate'] = TRUE;
      $visibility['request_path']['pages'] = $page;
    }
    $block->setVisibilityConfig('request_path', $visibility['request_path']);

    return $block->save();
  }

}
