CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

The HideBlock module provides the functionality to make blocks hidden instantly.

This module is intended for site-builders who are new to Drupal with relatively
simple needs. I shall try to accommodate feature requests and options but will
balance those with the need for a simple user interface.

 * For a full description of the module visit:
   https://www.drupal.org/project/hideblock

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/hideblock


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the HideBlock module as you would normally install a contributed
   Drupal module.
   Visit https://www.drupal.org/node/1897420 for further sinformation.


CONFIGURATION
-------------

    No configuration required.


TROUBLESHOOTING
---------------

If your blocks are not behaving as expected, ensure that the the browser
should support javascript, and if there any other JS file should
not have the conflicts with module JS. In addition, the [x]cross icon
have the separate CSS classes to work on its look and design.

MAINTAINERS
-----------

  * Shobhit Juyal (https://www.drupal.org/u/shobhit_juyal)

